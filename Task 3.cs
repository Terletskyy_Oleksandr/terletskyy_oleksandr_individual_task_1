﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Реалізувати клас для опису сутності «Трійковий вектор». Компоненти вектора приймають
// значення з множини {0,1,2}. Забезпечити клас потрібними конструкторами.Додати
// перевірку на ортогональність векторів. Додати метод, що визначатиме кількість компонент
// із значенням «2». Крім цього додати можливість виконувати операцію перетину двох
// неортогональних векторів (операція поелементного перетину неортогональних векторів
// здійснюється за правилом:  1⋂1=1⋂2=2⋂1=1, 0⋂0=0⋂2=2⋂0=0, 2⋂2=2).


// Allowed values of coordinates & intersection rules
public struct AllowedValues
{
    public static int[] Values
    {
        get { return new int[] { 0,1,2}; }
    }

    public static int Intersect(int x1, int x2)
    {
        if (!Values.Contains(x1) || !Values.Contains(x2))
        {
            throw new ArgumentException("Arguments must be in {0,1,2} range");
        }
        int result;
        switch (x1)
        {
            case 0:
                result = 0;
                break;
            case 1:
                switch (x2)
                {
                    case 0:
                        result = 0;
                        break;
                    default:
                        result = 1;
                        break;
                }
                break;
            default:
                switch (x2)
                {
                    case 0:
                        result = 0;
                        break;
                    case 1:
                        result = 1;
                        break;
                    default:
                        result = 2;
                        break;
                }
                break;
        }
        return result;
    }
}

// OOP representation of custom N dimensional vector
class NDimensionalVector : IEnumerable<int>
{
    //Components of the vector
    public int[] Coordinates { get; private set;}

    public int Dimension
    {
        get
        {
            return Coordinates.Length;
        }
    }

    public NDimensionalVector(int[] coordinates)
    {
        foreach (var item in coordinates)
        {
            if(!AllowedValues.Values.Contains(item))
            {
                throw new ArgumentException("Coordinates must be in {0,1,2} range");
            }
        }
        Coordinates = coordinates;
    }

    public static bool AreOrtogonal(NDimensionalVector vector1, NDimensionalVector vector2)
    {
        return ScalarProduct(vector1, vector2) == 0;
    }

    // Of unit intersection of respective elements
    public static NDimensionalVector InterSection(NDimensionalVector vec1, NDimensionalVector vec2)
    {
        var coordinates = Enumerable.Zip(vec1, vec2, (vec1Elem, vec2Elem) => AllowedValues.Intersect(vec1Elem, vec2Elem));
        return new NDimensionalVector(coordinates.ToArray());
    }

    // Returns number of 2s in Coordinates array
    public int CountOfTwo()
    {
        return (from coord in Coordinates where coord == 2 select coord).Count();
    }

    // Sum of multiplications of rspective elements from two vectors
    public static int ScalarProduct(NDimensionalVector vector1, NDimensionalVector vector2)
    {
        return Enumerable.Zip(vector1, vector2, (vec1, vec2) => vec1 * vec2).Sum();
    }

    public IEnumerator<int> GetEnumerator()
    {
        foreach (var item in Coordinates)
        {
            yield return item;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
    public override string ToString()
    {
        StringBuilder cordinatesString = new StringBuilder();
        bool firstCoord = true;
        foreach (var coord in Coordinates)
        {
            if (firstCoord)
            {
                firstCoord = false;
            }
            else
            {
                cordinatesString.Append(" ");
            }
            cordinatesString.Append(coord);
        }
        return cordinatesString.ToString();
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Instanciating
        Console.WriteLine("Creation of NDimensional vector instance (vect1)");
        var vect1 = new NDimensionalVector(new int[] { 1, 2, 0, 2, 1 });
        Console.WriteLine(vect1);
        Console.WriteLine();

        Console.WriteLine("Creation of another NDimensional vector instance (vect2)");
        var vect2 = new NDimensionalVector(new int[] { 2, 1, 0, 1, 2 });
        Console.WriteLine(vect2);
        Console.WriteLine();

        //Checking if these are ortogonal
        Console.WriteLine("vec1 is ortogonal to vec2: {0}",NDimensionalVector.AreOrtogonal(vect1,vect2));
        Console.WriteLine();

        //Count of 2s in a vector
        Console.WriteLine("Count of 2 in vect2");
        Console.WriteLine(vect2.CountOfTwo());
        Console.WriteLine();

        //Intersection of vect1 and vect2
        Console.WriteLine("Intersection of vect1 and vect2 using custom rules");
        Console.WriteLine(NDimensionalVector.InterSection(vect1,vect2));
        Console.WriteLine();

        //Delay
        Console.ReadKey();
    }
}

