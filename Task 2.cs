﻿using System;
using System.Linq;
using System.Text;

// Реалізувати клас для представлення многочленів від однієї змінної, що задаються
// степенем та масивом коефіцієнтів.Передбачити методи для обчислення значення
// многочлена для заданого аргументу, операції додавання, віднімання та множення многочленів
// з утворенням нового об’єкту-многочлена, вивід на екран многочлена.

// OOP representation of the polynom
class Polynom
{
    // Inner array that represents polynomical coefficients in respective order
    // [a0,a1,...]
    public double[] Coefficients
    {
        get;set;
    }

    // Max power of the polynom elements is equal to Count - 1 
    public int Count
    {
        get { return Coefficients.Length; }
    }

    public Polynom(params double[] coefficients)
    {
        Coefficients = coefficients;
        Split();
    }

    // String representation of the polynom
    public override string ToString()
    {
        StringBuilder polynomString = new StringBuilder();
        int pow = 0;
        bool firstNumber = true;
        foreach (var coeficient in Coefficients)
        {
            if (firstNumber)
            {
                firstNumber = false;
            }
            else
            {
                polynomString.Append("+");
            }
            polynomString.Append(coeficient);
            polynomString.Append("x^");
            polynomString.Append(pow);
            ++pow;
        }
        return polynomString.ToString();
    }

    // Value of the polynom depending on parameter value
    public double GetValue(double parameter)
    {
        double sum = 0;
        int pow = 0;
        foreach (var coefficient in Coefficients)
        {
            sum += coefficient * Math.Pow(parameter, pow++);
        }
        return sum;
    }

    // Remove zero coefficients from the end
    public void Split()
    {
        int countOfZeroCoficients = 0;
        for (int i = Count-1; i >= 0; i--)
        {
            if (Coefficients[i] != 0) break;
            ++countOfZeroCoficients;
        }
        double[] newCoeficients = new double[Count - countOfZeroCoficients];
        Array.Copy(Coefficients, newCoeficients, newCoeficients.Length);
        Coefficients = newCoeficients;
    }

    // Adds two polynoms
    static Polynom Add(Polynom firstPolynom,Polynom secondPolynom)
    {
        Polynom biggerPolynom, smallerPolynom;
        if (firstPolynom.Count>secondPolynom.Count)
        {
            biggerPolynom = firstPolynom;
            smallerPolynom = secondPolynom;
        }
        else
        {
            biggerPolynom = secondPolynom;
            smallerPolynom = firstPolynom;
        }

            double[] resultArray = new double[biggerPolynom.Count];
            biggerPolynom.Coefficients.CopyTo(resultArray, 0);
            for (int i = 0; i < smallerPolynom.Count; i++)
            {
                resultArray[i] += smallerPolynom.Coefficients[i];
            }
        var resultPolynom = new Polynom(resultArray);
        resultPolynom.Split();
        return resultPolynom;
    }

    // Multiplies every element by some value
    public static Polynom MultiplyByNumber(double number, Polynom p)
    {
        double[] newNumberArray = (from oldNumber in p.Coefficients select oldNumber * number).ToArray();
        return new Polynom(newNumberArray);
    }

    static Polynom Substract(Polynom firstPolynom, Polynom secondPolynom)
    {
        return firstPolynom + -1 * secondPolynom;
    }

    // Returns new polynom - multiplication of firstPolynom and secondPolynom
    static Polynom Multiply(Polynom firstPolynom, Polynom secondPolynom)
    {
        // Just multiply by first coefficient at first
        var resultPolynom = secondPolynom * firstPolynom.Coefficients[0];

        // Then multiply by other coefficients and add everything together
        for (int pow = 1; pow < firstPolynom.Count; pow++)
        {
            var preAdder = secondPolynom * firstPolynom.Coefficients[pow];

            // Now we have to increase the pow of the whole polynom
            double[] adder = new double[preAdder.Count + pow];
            preAdder.Coefficients.CopyTo(adder, pow);
            resultPolynom += new Polynom(adder);
        }
        return resultPolynom;
    }

    public static Polynom operator +(Polynom polynom1, Polynom polynom2)
    {
        return Add(polynom1, polynom2);
    }
    public static Polynom operator -(Polynom polynom1, Polynom polynom2)
    {
        return Substract(polynom1, polynom2);
    }
    public static Polynom operator *(double number, Polynom polynom)
    {
        return MultiplyByNumber(number, polynom);
    }
    public static Polynom operator *(Polynom polynom, double number)
    {
        return MultiplyByNumber(number, polynom);
    }
    public static Polynom operator *(Polynom polynom1, Polynom polynom2)
    {
        return Multiply(polynom1, polynom2);
    }

}

class Program
{
    static void Main(string[] args)
    {
        // Creation of oop polynom
        Console.WriteLine("Creation of the polynom with coefficients 1,2 (p1)");
        Polynom p1 = new Polynom(new double[] { 1, 2 });
        Console.WriteLine(p1);
        Console.WriteLine();

        Console.WriteLine("Creation of the polynom with coefficients 1,2,3,4,5 (p2)");
        Polynom p2 = new Polynom(new double[] { 1, 2,3 ,4 ,5 });
        Console.WriteLine(p2);
        Console.WriteLine();


        // Adition of 2 polynoms
        Console.WriteLine("Addition of two polynoms p1 and p2 ");
        Console.WriteLine(p1+p2);
        Console.WriteLine();

        // Substraction 
        Console.WriteLine("Substraction of polynoms p2 and p1 ");
        Console.WriteLine(p2-p1);
        Console.WriteLine();

        // Multiplication 
        Console.WriteLine("Multiplication of polynoms p2 and p1 ");
        Console.WriteLine(p2*p1);
        Console.WriteLine();

        Console.ReadKey();
    }
}

