﻿using System;

// Реалізувати клас для предcтавлення прямокутників зі сторонами, паралельними осям
// координат.Передбачити можливість переміщення прямокутників на площині, зміну
// розмірів, побудову найменшого прямокутника, що містить два заданих, а також –
// прямокутника, що є результатом перетину двох прямокутників.

class ParallelRectangle
{
    // A is still point when we change rectangle size
    // B is moving point during moving
    public Point A { get; private set; }
    public Point B { get; private set; }
        
    // 2 points enouth to describe our rectangle
    public ParallelRectangle(Point point1,Point point2)
    {
        if(point1.X==point2.X || point1.Y==point2.Y)
        {
            throw new ArgumentException("Points have to be not on the same line which is parallel to the one of the axels");
        }
        A = point1;B = point2;
    }

    //TODO: would be good to have also method to return new moved rectangle without changing current
    // moveVector describes increment of points of rectangle
    public void Move(Point moveVector)
    {
        A += moveVector;
        B += moveVector;
    }

    public void ChangeSize(Point newB)
    {
        B = newB;
    }

    // In next 2 methods we operate with projections of rectangles
    // on axels

    // Returns minimal possible rectangle that contains both rectangle1 and rectangle2
    public static ParallelRectangle TwoRectanglesContainer(ParallelRectangle rectangle1, ParallelRectangle rectangle2)
    {
        var rectangle1XRange = rectangle1.GetXRange();
        var rectangle2XRange = rectangle2.GetXRange();
        var rectangle1YRange = rectangle1.GetYRange();
        var rectangle2YRange = rectangle2.GetYRange();
        PositiveRange XUnion = PositiveRange.RangeUnion(rectangle1XRange, rectangle2XRange);
        PositiveRange YUnion = PositiveRange.RangeUnion(rectangle1YRange, rectangle2YRange);
        return new ParallelRectangle(new Point(XUnion.RangeStart, YUnion.RangeStart),
                                        new Point(XUnion.RangeEnd, YUnion.RangeEnd));

    }

    // Returns true if recranges intersect else false, also gives recrangle on intersection via out parameter
    public static bool TwoRectanglesIntersectionContainer(ParallelRectangle rectangle1, ParallelRectangle rectangle2, out ParallelRectangle result)
    {
        var rectangle1XRange = rectangle1.GetXRange();
        var rectangle2XRange = rectangle2.GetXRange();
        var rectangle1YRange = rectangle1.GetYRange();
        var rectangle2YRange = rectangle2.GetYRange();
        PositiveRange XIntersection;
        PositiveRange YIntersection;
        try
        {
            XIntersection = PositiveRange.RangeIntersection(rectangle1XRange, rectangle2XRange);
            YIntersection = PositiveRange.RangeIntersection(rectangle1YRange, rectangle2YRange);
        }
        catch (ArgumentException)
        {
            result = null;
            return false;
        }
        result = new ParallelRectangle(new Point(XIntersection.RangeStart, YIntersection.RangeStart),
                                        new Point(XIntersection.RangeEnd, YIntersection.RangeEnd));
        return true;
    }

    // Y projection via PositiveRange
    private PositiveRange GetYRange()
    {
        return (A.Y > B.Y) ? new PositiveRange(B.Y, A.Y) : new PositiveRange(A.Y, B.Y);
    }

    // X projection via PositiveRange
    private PositiveRange GetXRange()
    {
        return (A.X > B.X) ? new PositiveRange(B.X, A.X) : new PositiveRange(A.X, B.X);
    }

    // Whole 4 points of the rectangle
    private Point[] getPoints()
    {
        return new Point[] { A, B, new Point(A.X, B.Y), new Point(B.X, A.Y) };
    }

    // String representation of rectangle
    // 2 points that are not on the same parallel
    public override string ToString()
    {
        return string.Format("[({0},{1}),({2},{3})]",A.X,A.Y,B.X,B.Y);
    }
}

// Describes an segment from real axle
// in ascending order
public class PositiveRange
{
    public double RangeStart { get; private set; }
    public double RangeEnd { get; private set; }
    public PositiveRange(double xStart, double xEnd)
    {
        if (xStart >= xEnd) throw new ArgumentException("End of range has to be bigger then start");
        RangeStart = xStart;
        RangeEnd = xEnd;
    }
    public static PositiveRange RangeIntersection(PositiveRange range1,PositiveRange range2)
    {
        return new PositiveRange(Math.Max(range1.RangeStart,range2.RangeStart),
            Math.Min(range1.RangeEnd, range2.RangeEnd));
    }
    public static PositiveRange RangeUnion(PositiveRange range1, PositiveRange range2)
    {
        return new PositiveRange(Math.Min(range1.RangeStart, range2.RangeStart),
            Math.Max(range1.RangeEnd, range2.RangeEnd));
    }
}


public class Point
{
    public double X { get; private set; }
    public double Y { get; private set; }
    public Point(double x,double y)
    {
        X = x; Y = y;  
    }
    public static Point Add(Point point1, Point point2)
    {
        return new Point(point1.X + point2.X, point1.Y + point2.Y);
    }

    internal static double Distance(Point pointA, Point pointB)
    {
        return Math.Sqrt(Math.Pow(pointA.X - pointB.X, 2) +
                Math.Pow(pointA.Y - pointB.Y, 2));
    }

    public static Point operator +(Point point1, Point point2)
    {
        return Add(point1,point2);
    }
}

class Program
{
    static void Test()
    {
        ParallelRectangle rect1_original = new ParallelRectangle(new Point(0, 0), new Point(8, 8));
        ParallelRectangle rect2_original = new ParallelRectangle(new Point(6, 7), new Point(16, 17));

        // Instanciating
        Console.WriteLine("Creation of another ParallelRectangle instance (rect1)");
        ParallelRectangle rect1 = new ParallelRectangle(new Point(0, 0), new Point(8, 8));
        Console.WriteLine(rect1);
        Console.WriteLine();

        Console.WriteLine("Creation of ParallelRectangle instance (rect2)");
        ParallelRectangle rect2 = new ParallelRectangle(new Point(6, 7), new Point(16, 17));
        Console.WriteLine(rect2);
        Console.WriteLine();

        //Changing size and moving
        Console.WriteLine("Moving rect2 (Moving vector is (1,2) )");
        rect2.Move(new Point(1, 2));
        Console.WriteLine(rect2);
        Console.WriteLine();

        Console.WriteLine("Changing size of rect1 (new coordinates of running point are (10,10))");
        rect1.ChangeSize(new Point(10, 10));
        Console.WriteLine(rect1);
        Console.WriteLine();

        //Finding intersection of rectangles and minimal container for both of them
        Console.WriteLine("Intersection for rect1 and rect2");
        ParallelRectangle rect3;
        ParallelRectangle.TwoRectanglesIntersectionContainer(rect1_original, rect2_original, out rect3);
        Console.WriteLine((rect3 != null) ? rect3.ToString() : "Intersection does not exist");
        Console.WriteLine();


        ParallelRectangle rect1_intersection_not_exists = new ParallelRectangle(new Point(0, 0), new Point(8, 8));
        ParallelRectangle rect2_intersection_not_exists = new ParallelRectangle(new Point(9, 0), new Point(10, 10));
        Console.WriteLine("Test intersection not exists");
        ParallelRectangle rect4;
        ParallelRectangle.TwoRectanglesIntersectionContainer(rect1_intersection_not_exists, rect2_intersection_not_exists, out rect4);
        Console.WriteLine((rect4 != null) ? rect4.ToString() : "Intersection does not exist");
        Console.WriteLine();

        Console.WriteLine("Container for rectangles");
        rect3 = ParallelRectangle.TwoRectanglesContainer(rect1_original, rect2_original);
        Console.WriteLine(rect3);
        Console.WriteLine();

        //Delay
        Console.ReadKey();
    }

    static void Main(string[] args)
    {
        Test();

        // Instanciating
        Console.WriteLine("Creation of ParallelRectangle instance (rect1)");
        ParallelRectangle rect1 = new ParallelRectangle(new Point(0, 0), new Point(10, 10));
        Console.WriteLine(rect1);
        Console.WriteLine();

        Console.WriteLine("Creation of another ParallelRectangle instance (rect2)");
        ParallelRectangle rect2 = new ParallelRectangle(new Point(5, 5), new Point(15, 15));
        Console.WriteLine(rect2);
        Console.WriteLine();

        //Changing size and moving
        Console.WriteLine("Moving rect2 (Moving vector is (1,2) )");
        rect2.Move(new Point(1, 2));
        Console.WriteLine(rect2);
        Console.WriteLine();

        Console.WriteLine("Changing size of rect1 (new coordinates of running point are (8,8))");
        rect1.ChangeSize(new Point(8,8));
        Console.WriteLine(rect1);
        Console.WriteLine();

        //Finding intersection of rectangles and minimal container for both of them
        Console.WriteLine("Intersection for rect1 and rect2");
        ParallelRectangle rect3;
        ParallelRectangle.TwoRectanglesIntersectionContainer(rect1, rect2,out rect3);
        Console.WriteLine((rect3!=null) ? rect3.ToString():"Intersection does not exist");
        Console.WriteLine();

        Console.WriteLine("Container for rectangles");
        rect3 = ParallelRectangle.TwoRectanglesContainer(rect1, rect2);
        Console.WriteLine(rect3);
        Console.WriteLine();

        //Delay
        Console.ReadKey();
    }
}

