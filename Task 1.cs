﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Реалізувати клас для представлення матриці розміру m x n і будь-якого мінору в ній.
//Забезпечити клас потрібними конструкторами.Додати можливості обчислювати різного
//роду мінори (мінор к-го порядку, доповнювальний мінор до мінору к-го порядку, мінор
//елемента і-го рядка і j-го стовпця). Передбачити також основні операції(додавання,
//множення, ділення) над мінорами.

// OOP respesentation of matrix
class Matrix : IEnumerable<double[]>
{
    // Indexation of rows and columns starts from 1
    public double this[int row, int col]
    {
        get
        {
            if (row < 1 || row > Height || col < 1 || col > Width)
            {
                throw new IndexOutOfRangeException();
            }
            return matrixarray[row-1][col-1];
        }
        set
        {
            if (row < 1 || row > Height || col < 1 || col > Width)
            {
                throw new IndexOutOfRangeException();
            }
            matrixarray[row-1][col-1] = value;
        }
    }
    
    // Inner matrix data structure
    double[][] matrixarray;

    // Constructor by default, creates an empty matrix 
    public Matrix()
    {
        matrixarray = new double[0][];
    }

    // User constructor which creates height x width matrix filled with zeros;
    public Matrix(int height,int width)
    {
        if (height < 1 || width < 1)
        {
            throw new ArgumentException("Height and width of matrix must be 1 or higher");
        }
        matrixarray = new double[height][];
        for (int i = 0; i < height; i++)
        {
            matrixarray[i] = new double[width];
        }
    }

    public Matrix(double[][] juggedArray)
    {
        if (IsRectangle(juggedArray)) matrixarray = juggedArray;
        else throw new ArgumentException("Your array does not represent matrix");
    }

    // Creates an matrix from any collection of rows filled with double numbers
    public Matrix(IEnumerable<double[]> rowCollection)
        : this(ToJagged(rowCollection))
    {
    }

    public int Width
    {
        get
        {
            if (Height==0) return 0;
            return matrixarray[0].Length;
        }
    }

    public int Height
    {
        get
        {
            return matrixarray.Length;
        }
    }

    // String representation of matrix
    public override string ToString()
    {
        StringBuilder matrixString = new StringBuilder();
        // for better formating
        bool firstTimeRow = true;
        bool firstTimeNumber = true;

        foreach (var row in this)
        {
            if (firstTimeRow)
            {
                firstTimeRow = false;
            }
            else
            {
                matrixString.Append("\n");
            }
            foreach (var item in row)
            {
                if (firstTimeNumber)
                {
                    firstTimeNumber = false;
                }
                else
                {
                    matrixString.Append(" ");
                }
                matrixString.Append(item);    
            }
            firstTimeNumber = true;
        }
        return matrixString.ToString();
    }

    // Iterator returns next row of matrix in double[] on every iteration
    public IEnumerator<double[]> GetEnumerator()
    {
        foreach (var row in matrixarray)
        {
            yield return row;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    // Adition of matrixes is defined only for matrixes which have same size
    public static Matrix AddMatrixes(Matrix matrix1,Matrix matrix2)
    {
        if(matrix1.Height!=matrix2.Height || matrix1.Width!=matrix2.Width)
        {
            throw new ArgumentException("Both matrixes must have same size");
        }
        Matrix result = new Matrix();
        for (int row = 1; row <= matrix1.Height; row++)
        {
            double[] sumrow = new double[matrix1.Width];
            for (int col = 1; col <= matrix1.Width; col++)
            {
                sumrow[col - 1] = matrix1[row, col] + matrix2[row, col];
            }
            result.Add(sumrow);
        }
        return result;
    }

    public static Matrix Substract(Matrix matrix1, Matrix matrix2)
    {
        return AddMatrixes(matrix1, MultiplyByNumber(matrix2, -1));
    }

    // Multiplication of matrixes is defined only if
    // width of the first matrix is equal to the heigth of the second one
    public static Matrix Multiply(Matrix matrix1, Matrix matrix2)
    {
        if (matrix1.Width != matrix2.Height)
        {
            throw new ArgumentException("Width of the first matrix has to be equal to the heigth of the second one");
        }
        Matrix result = new Matrix();

        for (int row = 1; row <= matrix1.Height; row++)
        {
            double[] resultRow = new double[matrix2.Width];
            for (int col = 1; col <= matrix2.Width; col++)
            {
                for (int internalCol = 1; internalCol <= matrix1.Width; internalCol++)
                {    
                        resultRow[col - 1] += matrix1[row, internalCol] * matrix2[internalCol, col];   
                }
            }
            result.Add(resultRow);
        }
        return result;
    }

    // Multiplies every element of matrix by number
    public static Matrix MultiplyByNumber(Matrix matrix, double number)
    {
        Matrix result = new Matrix();
        for (int row = 1; row <= matrix.Height; row++)
        {
            for (int col = 1; col <= matrix.Width; col++)
            {
                matrix[row, col]*=number;
            }
        }
        return result;
    }

    // Calculates determinant of matrix created from elements
    // which are on intersection of rows and columns
    //
    // rows and columns contain numbers of rows or columns,
    // numeration starts from 1
    //
    // in case these numbers are wrong or length of rows and columns 
    // is not equal we will get ArgumentException from Submatrix 
    // and Determinant methods relatively
    public double CalculateMinor(int[] rows,int[] columns)
    {
        return Determinant(Submatrix(rows, columns));
    }

    // Calculates determinant of matrix created from elements
    // which are on intersection of rows and columns that are not
    // specified in rows and columns
    public double CalculateСomplementedMinor(int[] rows, int[] columns)
    {
        int[] targetRows = (from row in Enumerable.Range(1,Height)
                            where !rows.Contains(row) select row).ToArray();
        int[] targetColumns = (from column in Enumerable.Range(1, Width)
                            where !columns.Contains(column)
                            select column).ToArray();
        return Determinant(Submatrix(targetRows, targetColumns));
    }

    public double CalculateOneItemMinor(int row,int col)
    {
        // Or just return this[row,col];
        return Determinant(Submatrix(new int[] {row}, new int[] {col}));
    }

    public double CalculateOneItemСomplementedMinor(int row, int col)
    {
        return CalculateСomplementedMinor(new int[] {row}, new int[] {col});
    }

    // Calculate determinant by Laplace method
    // We decompose input matrix by its first row
    // and calculate respective determinants of submatrixes
    // recursively
    public static double Determinant(Matrix matrix)
    {
        if (!IsRectangle(matrix))
        {
            throw new ArgumentException("Determinant exists only for square matrix");
        }
        if( matrix.Height == 2 )
        {
            return matrix[1,1] * matrix[2,2] - matrix[1,2] * matrix[2,1];
        }
        if (matrix.Height == 1)
        {
            return matrix[1,1];
        }

        double Sum = 0;
        for (int firstRowElem = 1; firstRowElem <= matrix.Width; firstRowElem++)
        {
            double multiplier = (firstRowElem % 2 == 1) ? matrix[1, firstRowElem] : -1 * matrix[1, firstRowElem];

            int[] selectedRows = (from rowNumber in Enumerable.Range(2, matrix.Height - 1) 
                                  select rowNumber).ToArray();
            int[] selectedColumns = (from colNumber in Enumerable.Range(1,matrix.Width)
                                     where colNumber!= firstRowElem select colNumber).ToArray();
            Sum += multiplier * Determinant(matrix.Submatrix(selectedRows,selectedColumns));
        }
        return Sum;
    }

    // Adds row of doubles into matrix
    public void Add(double[] aditionalRow)
    {
        if (matrixarray.Length != 0 && aditionalRow.Length != Width)
        {
            throw new ArgumentException("Wrong number of elements in a row");
        }

        double[][] newMatrixArray = new double[Height + 1][];
        matrixarray.CopyTo(newMatrixArray, 0);
        newMatrixArray[Height] = aditionalRow;
        matrixarray = newMatrixArray;
    }

    private static bool IsRectangle(double[][] matrix)
    {
        int width = matrix[0].Length;
        foreach (var item in matrix)
        {
            if (item.Length != width)
            {
                return false;
            }
        }
        return true;
    }
    private static bool IsRectangle(Matrix matrix)
    {
        return matrix.Width == matrix.Height;
    }

    // Converts collection of rows double[] to jugged array
    private static double[][] ToJagged(IEnumerable<double[]> rowCollection)
    {
        var rowIterator = rowCollection.GetEnumerator();
        List<double[]> rowlist = new List<double[]>();
        foreach (var row in rowCollection)
        {
            rowlist.Add(row);
        }
        return rowlist.ToArray();
    }

    // Forms submatrix from elements that are on intersection of
    // certain rows and columns
    public Matrix Submatrix(int[] rowNumbers, int[] colNumbers)
    {
        if (rowNumbers.Length > Height || rowNumbers.Length < 1
            || colNumbers.Length > Width || colNumbers.Length < 1
            || rowNumbers.Max() > Height || rowNumbers.Min() < 1
            || colNumbers.Max() > Width || colNumbers.Min() < 1)
        {
            throw new ArgumentException("Wrong numbers of rows or columns");
        }

        Matrix SubMatrix = new Matrix();
        int rowslength = colNumbers.Length;
        foreach (int rowNumber in rowNumbers)
        {
            double[] nextArray = new double[rowslength];
            for (int i = 0; i < rowslength; i++)
            {
                nextArray[i] = matrixarray[rowNumber - 1][colNumbers[i] - 1];
            }
            SubMatrix.Add(nextArray);
        }
        return SubMatrix;
    }

    // Operators - wrappers of relative static methods

    public static Matrix operator +(Matrix matrix1,Matrix matrix2)
    {
        return AddMatrixes(matrix1, matrix2);
    }

    public static Matrix operator -(Matrix matrix1, Matrix matrix2)
    {
        return Substract(matrix1, matrix2);
    }

    public static Matrix operator *(Matrix matrix1, double number)
    {
        return MultiplyByNumber(matrix1, number);
    }

    public static Matrix operator *(double number, Matrix matrix1)
    {
        return MultiplyByNumber(matrix1, number);
    }

    public static Matrix operator *(Matrix matrix1, Matrix matrix2)
    {
        return Multiply(matrix1, matrix2);
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Creation of the matrix
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Different ways to create matrix instance");
        Console.WriteLine();
        Console.ForegroundColor = ConsoleColor.Gray;

        // 1st way (Initializator block)
        Console.WriteLine("Matrix created via initializator block (matrix) :");
        Matrix matrix = new Matrix(){ new double[] { 1, 2, 3 }, new double[] { 4, 5, 6 }, new double[] { 7, 8, 9 } };
        Console.WriteLine(matrix);
        Console.WriteLine();

        // 2nd way (Jagged array)
        Console.WriteLine("Matrix formed from array of arrays (matrix1) :");
        var matrix1 = new Matrix(new double[][] { new double[] {1,2 },new double[] {3,4} });
        Console.WriteLine(matrix1);
        Console.WriteLine();

        // 3rd way (Any collection of double[], for example list)
        Console.WriteLine("Matrix formed from List of arrays (matrix2) :");
        var matrix2 = new Matrix(new List<double[]> { new double[] { 5, 6 }, new double[] { 7, 8 } });
        Console.WriteLine(matrix2);
        Console.WriteLine();

        // 4th way (Manually via indexer, in that case all nodes of matrix are zero by default)
        Console.WriteLine("Matrix formed manually via indexer (matrix3) :");
        var matrix3 = new Matrix(3, 2);
        matrix3[1, 1] = 1; matrix3[3, 1] = 1; matrix3[2, 2] = 1;
        Console.WriteLine(matrix3);
        Console.WriteLine();

        //Some common operations with matrix
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Operation with matrixes");
        Console.ForegroundColor = ConsoleColor.Gray;
        Console.WriteLine();

        //Adition
        Console.WriteLine("Adition of the matrixes matrix1 and matrix 2");
        Console.WriteLine(matrix1+matrix2);
        Console.WriteLine();

        //Substraction
        Console.WriteLine("Substraction of the matrixes matrix1 and matrix 2");
        Console.WriteLine(matrix1 + matrix2);
        Console.WriteLine();

        //Multiplication
        Console.WriteLine("Multiplication of the matrixes matrix and matrix 3");
        Console.WriteLine(Matrix.Multiply(matrix,matrix3));
        Console.WriteLine();

        //Submatrix
        Console.WriteLine("Submatrix of the matrix where selected rows are 1,2 and selected column is 1");
        Console.WriteLine(matrix.Submatrix(new int[] {1,2},new int[] {1}));
        Console.WriteLine();

        //Determinant of matrix
        Console.WriteLine("Determinant of matrix");
        Console.WriteLine(Matrix.Determinant(matrix));
        Console.WriteLine();

        // Minors
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine("Calculation of minors & complemented minors");
        Console.ForegroundColor = ConsoleColor.Gray;
        Console.WriteLine();

        Console.WriteLine("Minor of matrix where selected rows are 1,3 and columns 1,2");
        Console.WriteLine("(minor of the second order)");
        Console.WriteLine(matrix.CalculateMinor(new int[] {1,3 },new int[] {1,2 }));
        Console.WriteLine();

        Console.WriteLine("Complemented minor of matrix where selected row is 3 and column 1 ");
        Console.WriteLine("(complmented minor of the first order)");
        Console.WriteLine(matrix.CalculateOneItemСomplementedMinor(3,1));
        Console.WriteLine();

        Console.ReadKey();
    }
}

